import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';

export default function Products() {

	const [ products, setProduct ] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			setProduct(data.map(product => {
				return (
					<ProductCard key={product._id} productProp = {product}/>
				)
			}))
		})
	}, [])

	return (
		<>
			<h1>Products</h1>
			{/*<div className="bg"></div>*/}
			{products}
		</>
	)
}