import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
		title : "Gourmet Tuyo",
		content : "Est. 2022",
		destination: "/products",
		label: "Order Now"
	}
	return (
		<>
			<Container>
	           <Banner data ={data}/>
	           <Highlights/>
	        </Container>
		</>
	)
}