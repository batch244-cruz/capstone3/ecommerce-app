import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {

  // Allows us to consume the User context object and its properties to use for user validation
  const { user, setUser } = useContext(UserContext);
    console.log(user)
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

    // Check if values are successfully binded
    console.log(email);
    console.log(password);

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.access !== undefined) {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                });
            } else {
                Swal.fire({
                    title: "Login Unsuccessful",
                    icon: "error",
                    text: "Email or password is incorrect"
                })
            }
        })

        setEmail('');
        setPassword('');

    }


    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);

    return (

        (user.id !== null)
        ?
            <Navigate to='/products'/>
        :
        
        <>
        <h1>Login</h1>
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId = "userEmail">
                <Form.Label> Email Address </Form.Label>
                <Form.Control
                    type="email"
                    placeholder = "Enter email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
            </Form.Group>

            <Form.Group controlId = "password">
                <Form.Label> Password </Form.Label>
                <Form.Control
                    type="password"
                    placeholder = "Password"
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group> <br/>

            {isActive
              ?
                <Button variant="success" type="submit" id="submitBtn"> Login </Button>
              :
                <Button variant="success" type="submit" id="submitBtn" disabled> Login </Button>
            }
        </Form>
        </>
    )
}