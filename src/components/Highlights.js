import { Row, Col, Card } from 'react-bootstrap';
import product1 from '../assets/classic.png';
import product2 from '../assets/sweetandspicy.png';
import product3 from '../assets/extraspicy.png';

export default function Highlights()  {
	return (
		
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				<img src={product2}/>
			      <Card.Body>
			        <Card.Title>
			        </Card.Title>
			     	<Card.Text>
			           A dried herring flakes soaked in olive oil (Sweet and Spicy flavor).
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				<img src={product1}/>
			      <Card.Body>
			        <Card.Title>

			        </Card.Title>
			     	<Card.Text>
			          A dried herring flakes soaked in olive oil (Original flavor).
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>		

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				<img src={product3}/>
			      <Card.Body>
			        <Card.Title>
			        </Card.Title>
			     	<Card.Text>
			          A dried herring flakes soaked in olive oil (Extra Spicy).
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}