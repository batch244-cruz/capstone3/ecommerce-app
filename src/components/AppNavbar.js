import { useContext } from 'react';
import { Navbar, Container, Nav} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return(

    <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/">Love Appetite</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to="/">Home</Nav.Link>
              
              {
                (user.isAdmin)
                ?
                <Nav.Link as={ NavLink } to="/admin" end>Admin</Nav.Link>
                :
                <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
              }

              {(user.id !== null)

                ?
                <>
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                </>
                :
                <>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register">Register</Nav.Link> 

                </>
              }

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

  )
}



// <Navbar bg="light" expand="lg">
//         <Container>
//           <Navbar.Brand as={Link} to="/">Love Appetite</Navbar.Brand>
//           <Navbar.Toggle aria-controls="basic-navbar-nav" />
//           <Navbar.Collapse id="basic-navbar-nav">
//             <Nav className="me-auto">
//               <Nav.Link as={NavLink} to="/">Home</Nav.Link>
              
//               {
//                 (user.isAdmin)
//                 ?
//                 <Nav.Link as={ NavLink } to="/admin" end>Admin Dashboard</Nav.Link>
//                 :
//                 <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
//               }

//               {(user.id !== null)

//                 ?
//                 <>
//                 <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
//                 <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
//                 </>
//                 :
//                 <>
//                   <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
//                   <Nav.Link as={NavLink} to="/register">Register</Nav.Link> 

//                 </>
//               }

//             </Nav>
//           </Navbar.Collapse>
//         </Container>
//       </Navbar>

    // <div>
    //   <nav className="navbar navbar-expand-lg">
    //     <a className="navbar-brand" href="/">Love Appetite</a>
    //       <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    //       <span className="navbar-toggler-icon"></span>
    //       </button>
    //         <div className="collapse navbar-collapse" id="navbarNav">
    //           <ul className="navbar-nav">
    //             <li className="nav-item">
    //             <a className="nav-link" href="#">Pricing</a>
    //             </li>
    //             <li className="nav-item">
    //             <a className="nav-link disabled" href="#">Disabled</a>
    //             </li>
    //           </ul>
    //         </div>
    //     </nav>
    // </div>    